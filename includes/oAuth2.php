<?php

class oAuth2 {

  public static function getProvidersDataList() {
    $providersDataList = array();

    foreach( glob( drupal_get_path('module', 'oauth2_connect') . '/plugins/*.php') as $filename ) {
      // Looking for class name here
      preg_match('|/(\w+_\w+)\.\w+$|is', $filename, $pock);
      $className = $pock[1];

      $providerObj = new $className();
      $providersDataList[] = array(
        'name' => $providerObj->getName(),
        'dialogUrl' => $providerObj->getDialogUrl(),
      );
    }

    return $providersDataList;
  }

}

abstract class oAuth2Abstract {

  abstract protected function getName();

  abstract protected function getDialogUrl();

  abstract protected function getUserDataArr($code);

  public function getCallbackUrl() {
    global $base_url;
    return urlencode($base_url . '/oauth2/connect/' . get_class($this));
  }

}
