<?php

class Oauth2_Facebook extends oAuth2Abstract {

  public $name = 'Facebook';

  private $app_id = "APP_ID";
  private $app_secret = "APP_SECRET";

  public function getName() {
    return $this->name;
  }

  public function getDialogUrl() {
    $dialog_url = 'https://www.facebook.com/dialog/oauth?client_id=' 
    . $this->app_id . '&redirect_uri=' . $this->getCallbackUrl() . '&scope=email';

    return $dialog_url;
  }

  public function getUserDataArr($code) {
    $token_url = "https://graph.facebook.com/oauth/access_token?"
    . "client_id=" . $this->app_id . "&client_secret=" . $this->app_secret . "&code=" . $code . '&redirect_uri=' . $this->getCallbackUrl();

    $responseStr = file_get_contents($token_url);
    if (!empty($responseStr)) {
      // Parses response and creates local variables
      parse_str($responseStr);

      $response = file_get_contents('https://graph.facebook.com/me?access_token=' . $access_token);
      $responseObj = json_decode($response);
    }

    if (!empty($responseObj->id)) {
      $userDataArr = array(
        'providerUid' => $responseObj->id,
        'providerName' => $this->name,
        'email' => $responseObj->email,
        'username' => substr($responseObj->email, 0, strpos($responseObj->email, '@')), 
        'accessToken' => $access_token,
      ); 
    }

    return $userDataArr;
  }
}

