<?php

class Oauth2_Yammer extends oAuth2Abstract  {

  public $name = 'Yammer';

  private $app_id = "APP_ID";
  private $app_secret = "APP_SECRET";

  public function getName() {
    return $this->name;
  }

  public function getDialogUrl() {
    $dialog_url = 'http://www.yammer.com/dialog/oauth?client_id=' 
    . $this->app_id . '&redirect_uri=' . $this->getCallbackUrl();

    return $dialog_url;
  }

  public function getUserDataArr($code) {
    $token_url = "https://www.yammer.com/oauth2/access_token.json?"
    . "client_id=" . $this->app_id . "&client_secret=" . $this->app_secret . "&code=" . $code;

    $response = file_get_contents($token_url);
    $responseObj = json_decode($response);

    if (!empty($responseObj->user->id)) {
      $userDataArr = array(
        'providerUid' => $responseObj->user->id,
        'providerName' => $this->name,
        'email' => $responseObj->user->contact->email_addresses[0]->address,
        'username' => $responseObj->user->name, 
        'accessToken' => $responseObj->access_token->token,
      ); 
    }

    return $userDataArr;
  }
  
}
