<?php

class Oauth2_Google extends oAuth2Abstract  {

  public $name = 'Google';

  private $app_id = "APP_ID";
  private $app_secret = "APP_SECRET";

  public function getName() {
    return $this->name;
  }

  public function getDialogUrl() {
    $dialog_url = 'https://accounts.google.com/o/oauth2/auth?client_id=' 
      . $this->app_id . '&redirect_uri=' . $this->getCallbackUrl() . '&response_type=code&scope='
      . 'https://www.googleapis.com/auth/userinfo.profile%20https://www.googleapis.com/auth/userinfo.email';

    return $dialog_url;
  }

  public function getUserDataArr($code) {

    $accessToken = $this->getAccessToken($code);

    $response = file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo?access_token=' . $accessToken);
    $responseObj = json_decode($response);

    if (!empty($responseObj->id)) {
      $userDataArr = array(
        'providerUid' => $responseObj->id,
        'providerName' => $this->name,
        'email' => $responseObj->email,
        'username' => substr($responseObj->email, 0, strpos($responseObj->email, '@')), 
        'accessToken' => $accessToken,
      ); 
    }

    return $userDataArr;
  }

  private function getAccessToken($code) {

    $tokenUrl = 'https://accounts.google.com/o/oauth2/token';
    $fieldStr = 'client_id=' . $this->app_id . '&client_secret=' . $this->app_secret . '&code=' . $code . '&redirect_uri=' . $this->getCallbackUrl() . '&grant_type=authorization_code';

    $ch = curl_init();
    
    //set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $tokenUrl);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldStr);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    
    //execute post
    $result = curl_exec($ch);
    $resultObj = json_decode($result);
    
    //close connection
    curl_close($ch);

    return $resultObj->access_token;

  }
  
}
